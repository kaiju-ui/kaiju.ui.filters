import {action, computed, observable, toJS} from "mobx";
import React from "react";
import {BooleanFilter, ListFilter, NumberRangeFilter} from "./FilterKinds";
import Axios from "axios";

class BaseQueryFilter {
    Component;

    @observable data;

    constructor(store, data) {
        this.store = store;
        this.idKey = store.idKey;
        this.key = data[store.idKey];
        this.data = data;
        this.kind = data.kind;
        this.label = data.label || `[${this.key}]`
    }

    get values() {
        // to check if there are some values in filter
        throw Error("Abstract method")
    }

    collectValue() {
        throw Error("Abstract method")
    }

    get selected() {
        return this.store.selectedValues[this.key]
    }

    @action
    setData(data) {
        this.data = data
    }
}

class BooleanFilterStore extends BaseQueryFilter {
    Component = BooleanFilter;
    @observable value = undefined;

    constructor(store, data) {
        super(store, data);
        this.value = data.value;
    }

    @computed
    get available() {
        let available = {};
        for (let v of this.data.values) {
            available[v[this.idKey]] = v.available;
        }
        return available
    }

    get values() {
        return this.data.values
    }

    @action
    onSelect(value) {
        this.value = value;
        this.store.selectedValues[this.key] = value;
        this.store.actions.onChangeCallback();
    }

    isSelected(value) {
        return this.selected === value
    }

    collectValue() {
        if (typeof (this.selected) == 'boolean') {
            return this.selected
        }

        return null
    }
}

class NumberRangeFilterStore extends BaseQueryFilter {
    Component = NumberRangeFilter;

    constructor(store, data) {
        super(store, data);

        if (!this.selected) {
            this.store.selectedValues[this.key] = {}
        }
    }

    get step() {
        if (this.data.kind.includes('decimal')) {
            return "0.01"
        }
        return "1"
    }

    @computed
    get available() {
        return this.data.available !== false
    }

    get values() {
        let values = [];
        if (this.maxValuePlaceholder) {
            values.push(this.minValuePlaceholder)
        }

        if (this.maxValuePlaceholder) {
            values.push(this.maxValuePlaceholder)

        }
        return values
    }

    @computed
    get minValuePlaceholder() {
        return this.data.values.min;
    }

    @computed
    get maxValuePlaceholder() {
        return this.data.values.max;
    }

    get minValue() {
        return this.selected?.minValue || ""
    }

    get maxValue() {
        return this.selected?.maxValue || ""
    }

    setSelected(value) {
        this.store.selectedValues[this.key] = value
    }

    @action
    setMin(value) {
        this.setSelected({...this.selected, minValue: value});
        this.store.actions.onChangeCallback();
    }

    @action
    setMax(value) {
        this.setSelected({...this.selected, maxValue: value});
        this.store.actions.onChangeCallback();
    }

    parse(value) {
        if (this.data.kind.includes('decimal_range')) {
            return parseFloat(value)
        } else {
            return parseInt(value)
        }
    }

    collectValue() {
        let values = {};

        if (this.selected.maxValue) {
            values['lte'] = this.parse(this.selected.maxValue)
        }

        if (this.selected.minValue) {
            values['gte'] = this.parse(this.selected.minValue)
        }

        return Object.keys(values).length > 0 ? values : null;
    }

}


class ListFilterStore extends BaseQueryFilter {
    @observable showMore = false;

    valuesToShow = 6;
    Component = ListFilter;

    constructor(store, data) {
        super(store, data);

        if (!this.selected) {
            this.store.selectedValues[this.key] = {}
        }
    }

    @action
    toggleShowMore() {
        this.showMore = !this.showMore
    }

    @computed
    get values() {
        if (!this.data.values) {
            return []
        }

        if (!this.showMore) {
            return this.data.values.slice(0, this.valuesToShow)
        }
        return this.data.values
    }

    @action
    toggleCheck(id) {
        if (!this.selected[id]) {
            this.store.selectedValues[this.key][id] = true
        } else {
            delete this.store.selectedValues[this.key][id]
        }

        this.store.actions.onChangeCallback();
    }

    isChecked(value) {
        return (this.selected || {})[value]
    }

    collectValue() {
        let keys = Object.keys(this.selected || {});
        return (keys.length === 0) ? null : keys;
    }

}

class QueryFiltersActions {
    @observable isFetching;
    timeout;

    constructor(store) {
        this.store = store;
        this.idKey = store.idKey;
    }

    onChangeCallback() {
        if (this.timeout) {
            clearTimeout(this.timeout)
        }

        this.timeout = setTimeout(() => {
            let filters = this.collectCheckedFilters();
            this.store.callback(filters);
        }, 300)
    }

    selectFilterStore(data) {
        switch (data.kind) {
            case 'string': {
                return new ListFilterStore(this.store, data)
            }
            case 'multiselect': {
                return new ListFilterStore(this.store, data)
            }
                case 'select': {
                return new ListFilterStore(this.store, data)
            }
            case 'decimal':
            case 'integer':
            case 'integer_range':
            case 'decimal_range': {
                return new NumberRangeFilterStore(this.store, data)
            }
            case 'boolean': {
                return new BooleanFilterStore(this.store, data)
            }
            default: {
                throw Error(`Store for kind ${data.kind} not found. data: ${data}`,)
            }
        }
    }

    collectCheckedFilters() {
        const filters = {};

        for (let store of this.store.filters) {
            let value = store.collectValue();

            if (value !== null) {
                filters[store.key] = value
            }
        }

        return filters
    }


    @action
    setNewFilters(filters) {
        let newFilters = [];
        let newFiltersMap = {};

        for (let i = 0; i < filters.length; i++) {
            let filter = filters[i];

            let filterStore = this.selectFilterStore(filter);
            newFiltersMap[filter[this.idKey]] = filterStore;
            newFilters.push(filterStore);
        }

        this.store.filtersMap = newFiltersMap;
        this.store.filters = newFilters;
    }

    @action
    updateFilters(filters) {
        let keys = this.store.filters.map(filter => filter.key);
        let filtersKeys = new Set(keys);
        let filtersKeysToDelete = new Set(keys);
        let newFiltersMap = {};
        let newFilters = [];

        for (let filter of filters) {
            let hasSomethingToAdd = !filtersKeys.has(filter[this.idKey]);
            let filterStore;
            filtersKeysToDelete.delete(filter[this.idKey]);

            if (hasSomethingToAdd) {
                // если такого фильтра нет, добавляем
                filterStore = this.selectFilterStore(filter);
                // поставить новый фильтр в начало
                this.store.filters.unshift(filterStore);
            } else {
                // если есть - обновляем
                filterStore = this.store.filtersMap[filter[this.idKey]]
                filterStore.setData(filter);
            }
            newFiltersMap[filter[this.idKey]] = filterStore;
            newFilters.push(filterStore)
        }

        this.store.filtersMap = newFiltersMap;

        if (filtersKeysToDelete.size > 0) {
            for (let key of filtersKeysToDelete.keys()) delete this.store.selectedValues[key];
            this.store.filters = newFilters
        } else {
            newFilters = null;
        }
    }

    @action
    fetchFilters(clean, update) {
        if (clean) {
            this.clean()
        }

        Axios.post("/public/rpc",
            {
                "method": this.store.conf.request.method,
                "params": this.store.conf.request.params
            }
        ).then(response => {
            let filters = response.data.result;

            if (!filters) {
                utils.handleNotifyExceptions(response);
                return
            }

            filters = Object.values(filters);

            if (update) {
                this.updateFilters(filters)
            } else {
                this.setNewFilters(filters)
            }

        }).catch(response => {
            this.isFetching = false;
            utils.handleNotifyExceptions(response)
        })
    }

    @action
    clean() {
        this.store.selectedValues = {};
    }
}

class FiltersStore {
    @observable filters;
    @observable selectedValues = {};

    filtersMap = {};

    constructor(conf) {
        this.conf = conf;
        this.idKey = "id" || conf.idKey;
        this.prefix = conf.prefix || "query_filters";
        this.callback = this.conf.callback;
        this.actions = new QueryFiltersActions(this)
    }

    setQuery(query) {
        if (!query) {
            this.conf.request.params["q"] = undefined;
            return
        }
        this.conf.request.params["q"] = query;
    }

    setFilters(filters) {
        this.conf.request.params.filters = filters
    }

    setFields(fields) {
        if (fields && fields.length === 0) {
            this.conf.request.params.fields = undefined;
            return
        }
        this.conf.request.params.fields = fields;
    }
}

export default FiltersStore;