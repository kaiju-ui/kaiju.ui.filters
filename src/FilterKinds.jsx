import {observer} from "mobx-react";
import React from "react";
import CheckboxComponent from "@kaiju.ui/components/src/Checkbox";
import InputComponent from "@kaiju.ui/components/src/Input";
import {RadioInput, RadioList} from "@kaiju.ui/components/src/Radio";

@observer
class BooleanFilter extends React.Component {
    render() {
        const store = this.props.store;

        return (
            <div className="pb-3 pr-4 pl-4">
                <div className="r-filter__header pb-1">
                    {store.label.capitalize()}
                </div>

                <RadioList>
                    <RadioInput
                        disabled={!store.available["1"]}
                        onChange={(e) => store.onSelect(true)}
                        checked={store.isSelected(true)}
                        label={utils.getTranslation("Filter.yes")}
                    />
                    <RadioInput
                        disabled={!store.available["0"]}
                        onChange={(e) => store.onSelect(false)}
                        checked={store.isSelected(false)}
                        label={utils.getTranslation("Filter.no")}/>
                    <RadioInput
                        disabled={!store.available["0"]}
                        onChange={(e) => store.onSelect(undefined)}
                        checked={store.isSelected(undefined)}
                        label={utils.getTranslation("Filter.no_matter")}/>
                </RadioList>
            </div>
        )
    }
}

class NumberRangeFilter extends React.Component {

    observableFromInput = observer(() => {
        let {store} = this.props;

        if (store.minRef) {
            store.minRef.placeholder = store.minValuePlaceholder;
        }

        return (
            <InputComponent
                type="number"
                ref={ref => store.minRef = ref}
                placeholder={store.minValuePlaceholder}
                step={store.step}
                readonly={!store.available}
                callback={(value) => store.setMin(value.value)}
                value={store.minValue}/>
        )
    });

    observableToInput = observer(() => {
        let {store} = this.props;


        if (store.maxRef) {
            store.maxRef.placeholder = store.maxValuePlaceholder
        }

        return (
            <InputComponent
                type="number"
                ref={ref => store.maxRef = ref}
                placeholder={store.maxValuePlaceholder}
                step={store.step}
                readonly={!store.available}
                callback={(value) => store.setMax(value.value)}
                value={store.maxValue}/>
        )
    });


    render() {
        let store = this.props.store;

        return (
            <div className="pb-3 pr-4 pl-4 r-filter__range__all">
                <div className="r-filter__header pb-1">
                    {store.label.capitalize()}
                </div>
                <div className="r-filter__range r-filter__range_left">
                    <span>{utils.getTranslation("Filter.from")}</span>
                    <this.observableFromInput/>
                </div>
                <div className="r-filter__range r-filter__range_right">
                    <span>{utils.getTranslation("Filter.to")}</span>
                    <this.observableToInput/>
                </div>
            </div>
        )
    }
}

@observer
class ListFilter extends React.Component {
    render() {
        const store = this.props.store;

        return (
            <div className="pb-3 pr-4 pl-4" key={utils.uuid4()}>
                <div className="r-filter__header pb-1">
                    {store.label.capitalize()}
                </div>
                <div>
                    <ul className="r-filter__list">
                        {
                            store.values.map((value) => {
                                    let key = value[store.idKey];
                                    return (
                                        <li key={utils.uuid4()}>
                                            <CheckboxComponent
                                                defaultChecked={store.isChecked(key)}
                                                className="r-filter__checkbox pb-3"
                                                label={value.label || `[${key}]`}
                                                onChange={() => store.toggleCheck(key)}
                                                disabled={!value.available}
                                            />
                                        </li>
                                    )
                                }
                            )
                        }
                    </ul>

                    {store.data.values.length > store.valuesToShow &&
                    <div className="r-filter__show_more grey text-center pointer"
                         onClick={() => store.toggleShowMore()}>
                        {!store.showMore && utils.getTranslation("Filter.show_more")}
                        {store.showMore && utils.getTranslation("Filter.hide")}
                    </div>
                    }

                </div>
            </div>
        )
    }
}

export {ListFilter, NumberRangeFilter, BooleanFilter}